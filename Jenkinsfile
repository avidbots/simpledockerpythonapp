pipeline {
    agent {
        docker {
            image '110513958225.dkr.ecr.us-east-1.amazonaws.com/avidbots/dev:1.8.0-rc3' 
            args '-u root --privileged'
        }
    }
    
    stages {

        stage("Setup") {
            steps {

                script {
                    sh 'printenv'
                    env.outputDir = "$WORKSPACE/$BUILD_ID/output"
                    echo "Creating output directory: $outputDir"
                    sh "mkdir -p $outputDir"
                }
                    
                timestamps {
                    sshagent (credentials: ['Bitbucket']) {
                        sh "mkdir ~/.ssh"
                        sh 'mkdir ~/.ccache'
                        sh 'sudo chmod -R 777 /etc/avidbots'
                        
                        sh "ssh-keyscan bitbucket.org > ~/.ssh/known_hosts"
                        sh 'echo "bitbucket.org,104.192.143.1 ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAubiN81eDcafrgMeLzaFPsw2kNvEcqTKl/VqLat/MaB33pZy0y3rJZtnqwR2qOOvbwKZYKiEO1O6VqNEBxKvJJelCq0dTXWT5pbO2gDXC6h6QDXCaHo6pOHGPUy+YBaGQRGuSusMEASYiWunYN0vCAI8QaXnWMXNMdFP3jHAJH0eDsoiGnLPBlBp4TNm6rYI74nMzgz3B9IikW4WVK+dc8KZJZWYjAuORU3jc1c/NPskD2ASinf8v3xnfXeukU0sJ5N6m5E8VLjObPEO+mN2t/FZTMZLiFqPWc/ALSqnMnnhwrNi2rbfg/rd/IpL8Le3pSBne8+seeFVBoGqzHM9yXw==" >> ~/.ssh/known_hosts'
                        sh "cat ~/.ssh/known_hosts"

                        sh '''#!/bin/bash -ex
                        # Create empty config files if they do not exist already
                        config_files=("/etc/avidbots/calibration.yaml"
                                    "/etc/avidbots/calibration.sh" 
                                    "/etc/avidbots/customer_settings.yaml"
                                    "/etc/avidbots/configuration_override.yaml" 
                                    "/etc/avidbots/configuration_override.sh")
                        sudo mkdir -p /etc/avidbots
                        for i in "${config_files[@]}"; do
                            if [ ! -f "$i" ]; then
                                touch "$i"
                            fi
                        done
                        '''
                        
                        sh "git clone -b master --depth=1 git@bitbucket.org:avidbots/robot-dev.git /home/avidbots/Dev/robot-dev"
                        sh "git clone  git@bitbucket.org:avidbots/autonomy.git /home/avidbots/Dev/robot-dev/repos/autonomy"

                        sh "ln -s  ../../repos/autonomy/ros_indigo/src /home/avidbots/Dev/robot-dev/catkin_ws/src/autonomy"

                        //sh 'cd ~/Dev/robot-dev/repos/autonomy && git submodule update --init'
                        sh "cd /home/avidbots/Dev/robot-dev/repos/autonomy && git log -n 1 --pretty=format:'%h'"
                        //sh "cd ~/Dev/robot-dev/repos/autonomy && git submodule"
                        
                        sh 'export PATH=/opt/ros/kinetic/bin:$PATH'
                   }
                }
            }
        }

        stage("Chekout and Merge") {
            steps {
                sshagent (credentials: ['Bitbucket']) {
                sh '''#!/bin/bash -e
                cd /home/avidbots/Dev/robot-dev/repos/autonomy
                echo "FETCH"
                git fetch --tags --progress git@bitbucket.org:avidbots/autonomy.git +refs/heads/*:refs/remotes/origin/*
                echo "CHECKOUT"
                git checkout $targetBranch
                echo "DELETE OLD BRANCHES"
                git reset --hard && git clean -ff -d
                echo "GIT CONFIG"
                git config user.name "Jenkins Autonomy CI"
                git config user.email jenkins@avidbots.com
                echo "MERGE"
                if `git merge origin/$sourceBranch | grep -q "CONFLICT"`; then 
                echo "\n==============================================================================="
                echo "Merge conflict between $sourceBranch and $targetBranch!"
                echo 'Please fix the conflicts!'
                echo "===============================================================================\n"
                exit 1
                fi
                echo "Initializing submodules..."
                git submodule update --init
                echo `git submodule`
                '''
                }
            }
        }

        stage("Fix dependencies") {
            steps {
                sh '''#!/bin/bash -e
                echo "\n==============================================================================="
                echo 'Updating apt sources list'
                echo "==============================================================================="
                sudo apt update -qq
                export ROS_DISTRO=kinetic
                cd /home/avidbots/Dev/robot-dev
                echo "\n==============================================================================="
                echo 'Updating ROS dependencies'
                echo "==============================================================================="
                rosdep update --include-eol-distros 
                echo "\n==============================================================================="
                echo 'Handling dependencies'
                echo "==============================================================================="
                ./scripts/deps/depHandler.py -b
                '''
            }
        }        
        
        stage("Build") {
            steps {
                timestamps {
                    sh '''#!/bin/bash -ex
                    source /home/avidbots/Dev/robot-dev/scripts/bashrc/ros.sh
                    source /home/avidbots/Dev/robot-dev/scripts/bashrc/ccache.sh
                    cd /home/avidbots/Dev/robot-dev/catkin_ws
                    catkin build --make-args -s -- -s --verbose
                    '''
                }
            }
        }
        
        stage("Test") {
            environment { 
                AVIDBOTS_ROBOT_MODEL = 'neo'
                AVIDBOTS_ROBOT_HARDWARE_VERSION='1_5'
                AVIDBOTS_ROBOT_CLEANING_WIDTH='24'
                AVIDBOTS_ROBOT_SQUEEGEE_WIDTH='small'
                CLEANING_HEAD_TYPE='cylindrical'
            }
            steps {
                timestamps {
                    sh '''#!/bin/bash 
                    export ROSCONSOLE_FORMAT='[${severity}] [${time}] [${node}] [${function}:${line}]:   ${message}'
                    
                    source /home/avidbots/Dev/robot-dev/scripts/bashrc/ros.sh
                    source /home/avidbots/Dev/robot-dev/scripts/bashrc/ccache.sh
                    cd /home/avidbots/Dev/robot-dev/catkin_ws
                    catkin build --make-args -s -- --summary --verbose --catkin-make-args run_tests

                    set +x
                    echo "\n\n==============================================================================="
                    echo "TEST SUMMARY"
                    echo "==============================================================================="
                    catkin_test_results --verbose
                    echo "==============================================================================="

                    echo "Creating output directory: $outputDir/test_results"
                    mkdir -p $outputDir/test_results

                    echo "Copying test results"
                    cp build/*/test_results/*/*.xml $outputDir/test_results
                    '''
                }
            }
        }
    }
    post {
        always {
            archiveArtifacts "${env.BUILD_ID}/output/test_results/*"
            junit "${env.BUILD_ID}/output/test_results/*.xml"
        }
    }
}
